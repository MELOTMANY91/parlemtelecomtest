﻿using CustomersProducts.Application.Products.Queries.GetCustomerProducts;
using CustomersProducts.Application.Products.Queries.GetProductsList;
using CustomersProducts.Domain.ViewModels;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace CustomersProducts.Api.Controllers
{
    [ApiController]
    [Route("api/[controller]/[action]")]
    public class ProductsController : BaseController
    {
        [HttpGet]
        [AllowAnonymous]
        public async Task<ActionResult<ProductsListVm>> GetAll()
        {
            var vm = await Mediator.Send(new GetProductsListQuery());

            return Ok(vm);
        }

        [HttpGet("{customerId}")]
        [ProducesResponseType(StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status404NotFound)]
        public async Task<ActionResult<CustomerProductsVm>> GetCustomerProducts(string customerId)
        {
            var vm = await Mediator.Send(new GetCustomerProductsQuery { CustomerId = customerId });

            return Ok(vm);
        }
    }
}
