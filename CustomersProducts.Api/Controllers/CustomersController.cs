﻿using CustomersProducts.Application.Customers.Queries;
using CustomersProducts.Domain.ViewModels;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;

namespace CustomersProducts.Api.Controllers
{
    [ApiController]
    [Route("api/[controller]/[action]")]
    public class CustomersController : BaseController
    {
        [HttpGet]
        [AllowAnonymous]
        public async Task<ActionResult<CustomersListVm>> GetAll()
        {
            var vm = await Mediator.Send(new GetCustomersListQuery());

            return Ok(vm);
        }
    }
}
