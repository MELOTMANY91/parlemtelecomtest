﻿using System;
using System.Collections.Generic;
using System.Text;

namespace CustomersProducts.Domain.Entities
{
    public class Product
    {
        public int ProductId { get; set; }
        public string ProductName { get; set; }
        public string ProductTypeName { get; set; }
        public long? NumeracioTerminal { get; set; }
        public DateTime? SoldAt { get; set; }
        public string CustomerId { get; set; }
    }
}
