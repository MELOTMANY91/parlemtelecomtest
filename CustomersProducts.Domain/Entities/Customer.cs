﻿using System;
using System.Collections.Generic;
using System.Text;

namespace CustomersProducts.Domain.Entities
{
    public class Customer
    {
        public int InternalId { get; set; }
        public string DocType { get; set; }
        public string DocNumber { get; set; }
        public string Email { get; set; }
        public string Id { get; set; }
        public string GivenName { get; set; }
        public string FamilyName { get; set; }
        public string Phone { get; set; }
    }
}
