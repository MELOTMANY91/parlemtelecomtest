﻿using CustomersProducts.Domain.Entities;
using System;
using System.Collections.Generic;
using System.Text;

namespace CustomersProducts.Domain.ViewModels
{
    public class CustomerProductsVm
    {
        public IList<Product> Products { get; set; }
    }
}
