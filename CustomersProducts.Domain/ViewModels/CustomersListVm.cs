﻿using CustomersProducts.Domain.Entities;
using System;
using System.Collections.Generic;
using System.Text;

namespace CustomersProducts.Domain.ViewModels
{
    public class CustomersListVm
    {
        public IList<Customer> Customers { get; set; }
    }
}
