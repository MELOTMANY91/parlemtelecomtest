﻿using CustomersProducts.Domain.ViewModels;
using Microsoft.AspNetCore.Components;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Threading.Tasks;

namespace CustomersProducts.WebUI.Data
{
    public class ProductService : IProductService
    {
        private readonly HttpClient _httpClient;

        public ProductService(HttpClient httpClient)
        {
            _httpClient = httpClient;
        }

        public async Task<ProductsListVm> GetAllProducts()
        {
            return await _httpClient.GetJsonAsync<ProductsListVm>("api/Products/GetAll");
        }

        public async Task<CustomerProductsVm> GetCustomerProducts(string custromerId)
        {
            return await _httpClient.GetJsonAsync<CustomerProductsVm>($"api/Products/GetCustomerProducts/{custromerId}");
        }
    }
}
