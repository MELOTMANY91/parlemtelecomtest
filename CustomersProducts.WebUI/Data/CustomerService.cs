﻿using CustomersProducts.Domain.ViewModels;
using Microsoft.AspNetCore.Components;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Threading.Tasks;

namespace CustomersProducts.WebUI.Data
{
    public class CustomerService : ICustomerService
    {
        private readonly HttpClient _httpClient;

        public CustomerService(HttpClient httpClient)
        {
            _httpClient = httpClient;
        }

        public async Task<CustomersListVm> GetCustomers()
        {
            return await _httpClient.GetJsonAsync<CustomersListVm>("api/Customers/GetAll");
        }
    }
}
