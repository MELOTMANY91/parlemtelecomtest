﻿using CustomersProducts.Domain.ViewModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace CustomersProducts.WebUI.Data
{
    public interface ICustomerService
    {
        Task<CustomersListVm> GetCustomers();
    }
}
