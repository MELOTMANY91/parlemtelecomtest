﻿using CustomersProducts.Domain.Entities;
using CustomersProducts.WebUI.Data;
using Microsoft.AspNetCore.Components;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace CustomersProducts.WebUI
{
    public class CustomersListBase : ComponentBase
    {
        [Inject]
        public ICustomerService CustomerService { get; set; }

        [Inject]
        public IProductService ProductService { get; set; }

        public IEnumerable<Customer> Customers { get; set; }

        public IEnumerable<Product> CustomerProducts { get; set; }

        protected override async Task OnInitializedAsync()
        {
            Customers = (await CustomerService.GetCustomers()).Customers.ToList();
        }

        public async void LoadCustomerProducts(ChangeEventArgs e)
        {
            CustomerProducts = (await ProductService.GetCustomerProducts(e.Value.ToString())).Products.ToList();
            StateHasChanged();
        }
    }
}
