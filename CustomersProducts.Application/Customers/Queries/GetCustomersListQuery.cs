﻿using CustomersProducts.Domain.Entities;
using CustomersProducts.Domain.ViewModels;
using MediatR;
using System;
using System.Collections.Generic;
using System.Text;

namespace CustomersProducts.Application.Customers.Queries
{
    public class GetCustomersListQuery : IRequest<CustomersListVm>
    {
    }
}
