﻿using CustomersProducts.Application.Common.Interfaces;
using CustomersProducts.Domain.Entities;
using CustomersProducts.Domain.ViewModels;
using MediatR;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace CustomersProducts.Application.Customers.Queries
{
    public class GetCustomersListQueryHandler : IRequestHandler<GetCustomersListQuery, CustomersListVm>
    {
        private readonly ICustomersProductsDbContext _context;

        public GetCustomersListQueryHandler(ICustomersProductsDbContext context)
        {
            _context = context;
        }

        public async Task<CustomersListVm> Handle(GetCustomersListQuery request, CancellationToken cancellationToken)
        {
            var customers = await _context.Customers.ToListAsync(cancellationToken);

            var vm = new CustomersListVm
            {
                Customers = customers
            };

            return vm;
        }
    }
}
