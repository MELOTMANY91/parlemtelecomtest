﻿using CustomersProducts.Application.Common.Exceptions;
using CustomersProducts.Application.Common.Interfaces;
using CustomersProducts.Domain.Entities;
using CustomersProducts.Domain.ViewModels;
using MediatR;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace CustomersProducts.Application.Products.Queries.GetCustomerProducts
{
    public class GetCustomerProductsQueryHandler : IRequestHandler<GetCustomerProductsQuery, CustomerProductsVm>
    {
        private readonly ICustomersProductsDbContext _context;

        public GetCustomerProductsQueryHandler(ICustomersProductsDbContext context)
        {
            _context = context;
        }

        public async Task<CustomerProductsVm> Handle(GetCustomerProductsQuery request, CancellationToken cancellationToken)
        {
            var products = await _context.Products.Where(x => x.CustomerId == request.CustomerId).ToListAsync(cancellationToken);

            var vm = new CustomerProductsVm
            {
                Products = products
            };

            return vm;
        }
    }
}
