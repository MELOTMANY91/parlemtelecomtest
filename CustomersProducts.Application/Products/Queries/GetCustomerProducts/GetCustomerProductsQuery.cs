﻿using CustomersProducts.Domain.ViewModels;
using MediatR;
using System;
using System.Collections.Generic;
using System.Text;

namespace CustomersProducts.Application.Products.Queries.GetCustomerProducts
{
    public class GetCustomerProductsQuery : IRequest<CustomerProductsVm>
    {
        public string CustomerId { get; set; }
    }
}
