﻿using CustomersProducts.Domain.ViewModels;
using MediatR;
using System;
using System.Collections.Generic;
using System.Text;

namespace CustomersProducts.Application.Products.Queries.GetProductsList
{
    public class GetProductsListQuery : IRequest<ProductsListVm>
    {
    }
}
