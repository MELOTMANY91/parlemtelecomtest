﻿using CustomersProducts.Application.Common.Interfaces;
using CustomersProducts.Domain.Entities;
using CustomersProducts.Domain.ViewModels;
using MediatR;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace CustomersProducts.Application.Products.Queries.GetProductsList
{
    public class GetProductsListQueryHandler : IRequestHandler<GetProductsListQuery, ProductsListVm>
    {
        private readonly ICustomersProductsDbContext _context;

        public GetProductsListQueryHandler(ICustomersProductsDbContext context)
        {
            _context = context;
        }

        public async Task<ProductsListVm> Handle(GetProductsListQuery request, CancellationToken cancellationToken)
        {
            var products = await _context.Products.ToListAsync(cancellationToken);

            var vm = new ProductsListVm
            {
                Products = products
            };

            return vm;
        }
    }
}
