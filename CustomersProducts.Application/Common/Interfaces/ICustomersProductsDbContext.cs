﻿using CustomersProducts.Domain.Entities;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Text;

namespace CustomersProducts.Application.Common.Interfaces
{
    public interface ICustomersProductsDbContext
    {
        DbSet<Customer> Customers { get; set; }

        DbSet<Product> Products { get; set; }
    }
}
