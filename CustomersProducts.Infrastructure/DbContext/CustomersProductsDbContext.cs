﻿using CustomersProducts.Application.Common.Interfaces;
using CustomersProducts.Domain.Entities;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Text;

namespace CustomersProducts.Infrastructure
{
    public class CustomersProductsDbContext : DbContext, ICustomersProductsDbContext
    {
        public CustomersProductsDbContext(DbContextOptions<CustomersProductsDbContext> options)
            : base(options)
        {
        }

        public DbSet<Customer> Customers { get; set; }
        public DbSet<Product> Products { get; set; }
    }
}
