﻿using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Text;

namespace CustomersProducts.Infrastructure
{
    public class CustomersProductsDbContextFactory : DesignTimeDbContextFactoryBase<CustomersProductsDbContext>
    {
        protected override CustomersProductsDbContext CreateNewInstance(DbContextOptions<CustomersProductsDbContext> options)
        {
            return new CustomersProductsDbContext(options);
        }
    }
}
