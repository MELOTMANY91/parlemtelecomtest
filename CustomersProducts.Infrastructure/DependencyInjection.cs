﻿using Microsoft.Extensions.DependencyInjection;
using System;
using System.Collections.Generic;
using System.Text;
using Microsoft.Extensions.Configuration;
using Microsoft.EntityFrameworkCore;
using CustomersProducts.Application.Common.Interfaces;

namespace CustomersProducts.Infrastructure
{
    public static class DependencyInjection
    {
        public static IServiceCollection AddInfrastructure(this IServiceCollection services, IConfiguration configuration)
        {
            services.AddDbContext<CustomersProductsDbContext>(options =>
                options.UseSqlite(configuration.GetConnectionString("Database")));

            services.AddScoped<ICustomersProductsDbContext>(provider => provider.GetService<CustomersProductsDbContext>());

            return services;
        }
    }
}
