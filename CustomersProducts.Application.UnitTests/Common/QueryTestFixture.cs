﻿using CustomersProducts.Infrastructure;
using System;
using System.Collections.Generic;
using System.Text;
using Xunit;

namespace CustomersProducts.Application.UnitTests.Common
{
    public class QueryTestFixture : IDisposable
    {
        public CustomersProductsDbContext Context { get; private set; }

        public QueryTestFixture()
        {
            Context = CustomersProductsContextFactory.Create();

        }

        public void Dispose()
        {
            CustomersProductsContextFactory.Destroy(Context);
        }
    }

    [CollectionDefinition("QueryCollection")]
    public class QueryCollection : ICollectionFixture<QueryTestFixture> { }
}
