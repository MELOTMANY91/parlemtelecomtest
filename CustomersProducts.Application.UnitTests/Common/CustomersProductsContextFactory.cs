﻿using CustomersProducts.Domain.Entities;
using CustomersProducts.Infrastructure;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Text;

namespace CustomersProducts.Application.UnitTests.Common
{
    public class CustomersProductsContextFactory
    {
        public static CustomersProductsDbContext Create()
        {
            var options = new DbContextOptionsBuilder<CustomersProductsDbContext>()
                .UseInMemoryDatabase(Guid.NewGuid().ToString())
                .Options;

            var context = new CustomersProductsDbContext(options);

            context.Database.EnsureCreated();

            context.Customers.AddRange(new[] {
                new Customer { InternalId = 1, Id = "11111", FamilyName = "Parlem" },
                new Customer { InternalId = 2, Id = "22222", FamilyName = "El Otmany" }
            });

            context.Products.Add(new Product
            {
                ProductId = 111,
                CustomerId = "11111"
            });

            context.Products.Add(new Product
            {
                ProductId = 222,
                CustomerId = "11111"
            });

            context.Products.Add(new Product
            {
                ProductId = 333,
                CustomerId = "22222"
            });

            context.SaveChanges();

            return context;
        }

        public static void Destroy(CustomersProductsDbContext context)
        {
            context.Database.EnsureDeleted();

            context.Dispose();
        }
    }
}
