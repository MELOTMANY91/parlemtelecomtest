﻿using CustomersProducts.Application.Customers.Queries;
using CustomersProducts.Application.UnitTests.Common;
using CustomersProducts.Infrastructure;
using System;
using System.Collections.Generic;
using System.Text;
using Shouldly;
using System.Threading;
using System.Threading.Tasks;
using Xunit;
using CustomersProducts.Domain.ViewModels;

namespace CustomersProducts.Application.UnitTests.Customers.Queries
{
    [Collection("QueryCollection")]
    public class GetCustomersListQueryHandlerTests
    {
        private readonly CustomersProductsDbContext _context;

        public GetCustomersListQueryHandlerTests(QueryTestFixture fixture)
        {
            _context = fixture.Context;
        }

        [Fact]
        public async Task GetCustomersTest()
        {
            var sut = new GetCustomersListQueryHandler(_context);

            var result = await sut.Handle(new GetCustomersListQuery(), CancellationToken.None);

            result.ShouldBeOfType<CustomersListVm>();

            result.Customers.Count.ShouldBe(2);
        }
    }
}
