﻿using CustomersProducts.Application.Products.Queries.GetCustomerProducts;
using CustomersProducts.Application.UnitTests.Common;
using CustomersProducts.Domain.ViewModels;
using CustomersProducts.Infrastructure;
using Shouldly;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using Xunit;

namespace CustomersProducts.Application.UnitTests.Products.Queries
{
    [Collection("QueryCollection")]
    public class GetCustomerProductsQueryHandlerTests
    {
        private readonly CustomersProductsDbContext _context;

        public GetCustomerProductsQueryHandlerTests(QueryTestFixture fixture)
        {
            _context = fixture.Context;
        }

        [Fact]
        public async Task GetCustomerProductsTest()
        {
            var sut = new GetCustomerProductsQueryHandler(_context);

            var result = await sut.Handle(new GetCustomerProductsQuery() { CustomerId = "11111"}, CancellationToken.None);

            result.ShouldBeOfType<CustomerProductsVm>();

            result.Products.Count.ShouldBe(2);
        }
    }
}
