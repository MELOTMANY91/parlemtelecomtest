﻿using CustomersProducts.Application.UnitTests.Common;
using CustomersProducts.Infrastructure;
using System;
using System.Collections.Generic;
using System.Text;
using Shouldly;
using System.Threading;
using System.Threading.Tasks;
using Xunit;
using CustomersProducts.Application.Products.Queries;
using CustomersProducts.Application.Products.Queries.GetProductsList;
using CustomersProducts.Domain.ViewModels;

namespace CustomersProducts.Application.UnitTests.Products.Queries
{
    [Collection("QueryCollection")]
    public class GetProductsListQueryHandlerTests
    {
        private readonly CustomersProductsDbContext _context;

        public GetProductsListQueryHandlerTests(QueryTestFixture fixture)
        {
            _context = fixture.Context;
        }

        [Fact]
        public async Task GetCustomersTest()
        {
            var sut = new GetProductsListQueryHandler(_context);

            var result = await sut.Handle(new GetProductsListQuery(), CancellationToken.None);

            result.ShouldBeOfType<ProductsListVm>();

            result.Products.Count.ShouldBe(3);
        }
    }
}
